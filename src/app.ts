import express from 'express';
import bodyParser from 'body-parser';
import salesRoutes from "./routes/salesRoutes.js";

const app = express();

app.use(bodyParser.json());
app.use("/sales", salesRoutes);
