import "reflect-metadata";
import { createConnection } from "typeorm";
import { Car } from "../entities/Car.js";
import { Customer } from "../entities/Customer.js";
import { Sale } from "../entities/Sale.js";

createConnection()
    .then(async (connection) => {
        const cars = [
            { name: "BMW X5" },
            { name: "BMW X6" },
            { name: "BMW X7" },
        ];
        const carEntities = await Promise.all(
            cars.map(async (car) => {
                const carEntity = new Car();
                carEntity.model = car.name;
                return await connection.manager.save(carEntity);
            })
        );

        const customers = [
            {
                name: "Иванов Сергей",
                phone: "+79107891122",
            },
            {
                name: "Коробкин Олег",
                phone: "+79107891155",
            },
            {
                name: "Олейкин Роман",
                phone: "+79107891166",
            },
        ];
        const customerEntities = await Promise.all(
            customers.map(async (customer) => {
                const customerEntity = new Customer();
                customerEntity.name = customer.name;
                customerEntity.phone = customer.phone;
                return await connection.manager.save(customerEntity);
            })
        );

        const sales = [
            {
                car: carEntities[0],
                amount: 1,
                totalPrice: 2000000,
                date: new Date("2022-07-01"),
                customer: customerEntities[0],
            },
            {
                car: carEntities[1],
                amount: 2,
                totalPrice: 3500000,
                date: new Date("2022-07-02"),
                customer: customerEntities[1],
            },
            {
                car: carEntities[2],
                amount: 1,
                totalPrice: 2000000,
                date: new Date("2022-07-02"),
                customer: customerEntities[2],
            },
            {
                car: carEntities[2],
                amount: 1,
                totalPrice: 2000000,
                date: new Date("2022-07-02"),
                customer: customerEntities[1],
            },
            {
                car: carEntities[0],
                amount: 2,
                totalPrice: 2000000,
                date: new Date("2022-07-02"),
                customer: customerEntities[1],
            },
            {
                car: carEntities[1],
                amount: 1,
                totalPrice: 3000000,
                date: new Date("2022-07-03"),
                customer: customerEntities[0],
            },
        ];
        await Promise.all(
            sales.map(async (sale) => {
                const saleEntity = new Sale();
                saleEntity.car = sale.car;
                saleEntity.amount = sale.amount;
                saleEntity.totalPrice = sale.totalPrice;
                saleEntity.date = sale.date;
                saleEntity.customer = sale.customer;
                await connection.manager.save(saleEntity);
            })
        );
    })
    .catch((error) => console.log(error));
