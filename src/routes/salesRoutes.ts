import express from "express";
import { getOrders, getRevenueByPeriod } from "../controllers/salesController.js";

const router = express.Router();

router.get("/revenue", getRevenueByPeriod);
router.get("/orders", getOrders);

export default router;
