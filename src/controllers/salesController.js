"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getOrders = exports.getRevenueByPeriod = exports.getSales = void 0;
const Sale_1 = require("../entities/Sale");
const ormconfig_1 = __importDefault(require("../config/ormconfig"));
const typeorm_1 = require("typeorm");
const getSales = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const salesRepository = ormconfig_1.default.getRepository(Sale_1.Sale);
    const sales = yield salesRepository.find({ relations: ["customer"] });
    res.json(sales);
});
exports.getSales = getSales;
const getRevenueByPeriod = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { startDate, endDate } = req.query;
    const salesRepository = ormconfig_1.default.getRepository(Sale_1.Sale);
    const sales = yield salesRepository.find({
        where: {
            date: (0, typeorm_1.Between)(new Date(startDate), new Date(endDate)),
        },
        relations: ['customer'],
    });
    const revenueByCustomer = {};
    sales.forEach((sale) => {
        const customerId = sale.customer.id;
        const revenue = sale.totalPrice;
        if (revenueByCustomer[customerId]) {
            revenueByCustomer[customerId] += revenue;
        }
        else {
            revenueByCustomer[customerId] = revenue;
        }
    });
    return res.json(revenueByCustomer);
});
exports.getRevenueByPeriod = getRevenueByPeriod;
const getOrders = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { customerId } = req.params;
    const salesRepository = ormconfig_1.default.getRepository(Sale_1.Sale);
    const sales = yield salesRepository.find({
        where: {
            customer: { id: Number(customerId) },
        },
        relations: ['customer'],
    });
    return res.json(sales);
});
exports.getOrders = getOrders;
