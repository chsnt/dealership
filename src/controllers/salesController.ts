import { Request, Response } from "express";
import { Sale } from "../entities/Sale.js";
import connectDB from "../config/ormconfig.js";
import { Between } from "typeorm";

export const getSales = async (req: Request, res: Response) => {
    const salesRepository = connectDB.getRepository(Sale);
    const sales = await salesRepository.find({ relations: ["customer"] });
    res.json(sales);
};

export const getRevenueByPeriod = async (req: Request, res: Response) => {
    const { startDate, endDate } = req.query;
    const salesRepository = connectDB.getRepository(Sale);

    const sales = await salesRepository.find({
        where: {
            date: Between(
                new Date(startDate as string),
                new Date(endDate as string),
            ),
        },
        relations: ['customer'],
    });

    const revenueByCustomer: { [key: string]: number } = {};

    sales.forEach((sale) => {
        const customerId = sale.customer.id;
        const revenue = sale.totalPrice;

        if (revenueByCustomer[customerId]) {
            revenueByCustomer[customerId] += revenue;
        } else {
            revenueByCustomer[customerId] = revenue;
        }
    });

    return res.json(revenueByCustomer);
}

export const getOrders = async (req: Request, res: Response) => {
    const { customerId } = req.params;
    const salesRepository = connectDB.getRepository(Sale);

    const sales = await salesRepository.find({
        where: {
            customer: { id: Number(customerId) },
        },
        relations: ['customer'],
    });

    return res.json(sales);
}
