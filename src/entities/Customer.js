"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Customer = void 0;
const typeorm_1 = require("typeorm");
const Sale_1 = require("./Sale");
let Customer = class Customer {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)()
], Customer.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)()
], Customer.prototype, "firstName", void 0);
__decorate([
    (0, typeorm_1.Column)()
], Customer.prototype, "lastName", void 0);
__decorate([
    (0, typeorm_1.Column)()
], Customer.prototype, "phone", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => Sale_1.Sale, sale => sale.customer)
], Customer.prototype, "sales", void 0);
Customer = __decorate([
    (0, typeorm_1.Entity)()
], Customer);
exports.Customer = Customer;
