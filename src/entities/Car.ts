import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import { Sale } from "./Sale.js";

@Entity()
export class Car {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    model!: string;

    @Column({ name: "year" })
    year!: number;

    @Column({ name: "price" })
    price!: number;

    @ManyToOne(() => Sale, sale => sale.carId)
    sale!: Sale;

}
