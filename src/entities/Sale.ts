import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm';
import { Customer } from './Customer.js';
import { Car } from "./Car.js";

@Entity()
export class Sale {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    amount!: number;

    @Column()
    totalPrice!: number;

    @Column({ type: 'date' })
    date!: Date;

    @ManyToOne(() => Customer, customer => customer.sales)
    customer!: Customer;

    @ManyToOne(() => Car, car => car.id)
    @JoinColumn({ name: 'carId' })
    carId!: Car;
}
