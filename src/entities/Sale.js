"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Sale = void 0;
const typeorm_1 = require("typeorm");
const Customer_1 = require("./Customer");
let Sale = class Sale {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)()
], Sale.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)()
], Sale.prototype, "carName", void 0);
__decorate([
    (0, typeorm_1.Column)()
], Sale.prototype, "amount", void 0);
__decorate([
    (0, typeorm_1.Column)()
], Sale.prototype, "totalPrice", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'date' })
], Sale.prototype, "date", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Customer_1.Customer, customer => customer.sales)
], Sale.prototype, "customer", void 0);
Sale = __decorate([
    (0, typeorm_1.Entity)()
], Sale);
exports.Sale = Sale;
