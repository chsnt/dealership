import {MigrationInterface, QueryRunner} from "typeorm";

export class Initial implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            CREATE TABLE IF NOT EXISTS car (
                id SERIAL PRIMARY KEY,
                make VARCHAR(255) NOT NULL,
                model VARCHAR(255) NOT NULL,
                year INT NOT NULL,
                price DECIMAL(10, 2) NOT NULL
            );
            
            CREATE TABLE IF NOT EXISTS customer (
                id SERIAL PRIMARY KEY,
                name VARCHAR(255) NOT NULL,
                email VARCHAR(255) NOT NULL UNIQUE,
                phone VARCHAR(20) NOT NULL
            );
            
            CREATE TABLE IF NOT EXISTS sale (
                id SERIAL PRIMARY KEY,
                car_id INT NOT NULL REFERENCES car(id),
                customer_id INT NOT NULL REFERENCES customer(id),
                price DECIMAL(10, 2) NOT NULL,
                sold_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
            );
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            DROP TABLE IF EXISTS sale;
            DROP TABLE IF EXISTS customer;
            DROP TABLE IF EXISTS car;
        `);
    }

}
